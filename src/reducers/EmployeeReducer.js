import { GET_ALL_EMPLOYEE, EDIT_EMPLOYEE_INFO, CLEAR_EMPLOYEE_INFO, GET_TOKEN } from "../actions/types";

const initialState = [];
export function getEmployeeReducer(state = initialState , action){
    switch(action.type){
        case GET_ALL_EMPLOYEE:
            if(action.payload.status === 200){
                return action.payload.body;
            }
            return initialState;
        default: return state;
    }
};

export function isEmployeeEditedReducer(state = null , action){
    switch(action.type){
        case EDIT_EMPLOYEE_INFO:
            if(action.payload.status === 200){
                return true;
            }
            return false;
        case CLEAR_EMPLOYEE_INFO:
           return null;
        default: return state;
    }
};

export function tokenReducer(state = null , action){
    switch(action.type){
        case GET_TOKEN:
            if(action.payload.status === 200){
                return action.payload.body;
            }
            return null;
        default: return state;
    }
};
