import { combineReducers } from 'redux';
import {
    getEmployeeReducer,
    isEmployeeEditedReducer,
    tokenReducer
} from './EmployeeReducer';

const rootReducer = combineReducers({
    allEmployees: getEmployeeReducer,
    isEmployeeEdited: isEmployeeEditedReducer,
    token: tokenReducer
});

export default rootReducer;