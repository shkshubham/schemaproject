import {
    getEmployeesAction,
    getEmployeesTokenAction,
    editEmployeesAction,
    clearEmployeesAction
} from './EmployeeAction';

export const getEmployees = getEmployeesAction;
export const getToken = getEmployeesTokenAction;
export const editEmployee = editEmployeesAction;
export const clearEmployee = clearEmployeesAction;