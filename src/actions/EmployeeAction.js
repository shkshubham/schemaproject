import { getEmployeeAPI, getEmployeeTokenAPI, editEmpolyeeAPI } from '../apis/EmployeeAPI';
import { GET_ALL_EMPLOYEE, EDIT_EMPLOYEE_INFO, CLEAR_EMPLOYEE_INFO, GET_TOKEN } from './types';
export function getEmployeesAction(employee_id) {
    const payload = getEmployeeAPI(employee_id);
    return {
        type: GET_ALL_EMPLOYEE,
        payload
    }
}

export function getEmployeesTokenAction(data) {
    const payload = getEmployeeTokenAPI(data);
    return {
        type: GET_TOKEN,
        payload
    }
}

export function editEmployeesAction(data) {
    const payload = editEmpolyeeAPI(data);
    return {
        type: EDIT_EMPLOYEE_INFO,
        payload
    }
}

export function clearEmployeesAction(data) {
    return {
        type: CLEAR_EMPLOYEE_INFO,
    }
}
