import React, { Component } from 'react';
import './employee.css';
import { Row, Col, Table, Button } from 'react-bootstrap';
import { getEmployees, getToken, editEmployee, clearEmployee } from '../../actions/actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
class Employee extends Component {

    constructor(props){
        super(props);
        this.state = {
            username: "6946DFE8F2E6457592870B31652AE2A8",
            tableHeader: [ "Id", "Name", "Surname", "Action" ],
            selectedEmployee: null,
            employeeData: null
        }
    }

    renderTableHeader = () => {
        const {
            tableHeader
        } = this.state;
        return tableHeader.map(header => {
            return (
                <th key={header}>{header}</th>
            )
        })
    }
    componentDidMount(){
        this.props.getEmployees("6946DFE8F2E6457592870B31652AE2A8");
        this.props.getToken({
            username: this.state.username,
            password: "AFDB8E5"
        })
    }
    employeeSelected = (employee) => {
        this.setState({
            selectedEmployee: employee,
            employeeData: employee
        })
    }
    changeEmployeeData = ({ target }, key) => {
        const {
            value
        } = target
        const {employeeData} = this.state;
        employeeData[key] = value;
        this.setState({
            employeeData
        })
    }
    submitEmployeeBtn = () => {
        const {
            username
        } = this.state;
        const {
            editEmployee,
            clearEmployee,
            token
        } = this.props;
        const {
            employeeData,
        } = this.state;
        clearEmployee();
        editEmployee({
            employee_id: username,
            token: token.access_token,
            data: employeeData
        })
    }
    clearData = () => {
        this.setState({
            selectedEmployee: null,
            employeeData: null
        })
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.isEmployeeEdited && this.props.isEmployeeEdited !== nextProps.isEmployeeEdited){
            alert("Employee Added");
            this.clearData()
        }
        else if(nextProps.isEmployeeEdited === false && this.props.isEmployeeEdited !== nextProps.isEmployeeEdited){
            this.props.getEmployees();
            this.clearData()
            alert("Error Ocurred");
        }
    }
    renderTableBody = () => {
        return this.props.allEmployees.map(employee => {
            const {
                Id,
                Name,
                Surname
            } = employee;
            const { selectedEmployee, employeeData } = this.state;
            if(selectedEmployee && selectedEmployee.Id === Id){
                return (
                    <tr className="employee-tr" key={Id}>
                        <td><input 
                            type="text" 
                            onChange={(e) => this.changeEmployeeData(e, 'Id')} 
                            value={employeeData.Id} />
                        </td>
                        <td><input 
                            type="text" 
                            onChange={(e) => this.changeEmployeeData(e, 'Name')} 
                            value={employeeData.Name} />
                        </td>
                        <td><input 
                            type="text" 
                            onChange={(e) => this.changeEmployeeData(e, 'Surname')} 
                            value={employeeData.Surname} />
                        </td>
                        <td><Button onClick={() => this.submitEmployeeBtn()} type="submit">Save</Button></td>
                    </tr>
                )
            }else{
                return (
                    <tr className="employee-tr" key={Id}>
                        <td>{Id}</td>
                        <td>{Name}</td>
                        <td>{Surname}</td>
                        <td><Button onClick={() => this.employeeSelected(employee)} >Edit</Button></td>
                    </tr>
                )
            }

        })
        
    }
    render() {
        return (
            <Row style={{
                marginTop: 100
            }}>
                <Col xs={12}>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            { this.renderTableHeader() }
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.allEmployees ? this.renderTableBody() : "Loading"
                        }
                    </tbody>
                </Table>
                </Col>
            </Row>
        );
  }
}

function mapStateToProps(state) {
    const {
        allEmployees,
        token,
        isEmployeeEdited
    } = state;
    return {
        allEmployees,
        token,
        isEmployeeEdited
    }
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getEmployees,
        getToken,
        editEmployee,
        clearEmployee
    }, dispatch);
  }

export default connect(mapStateToProps,mapDispatchToProps)(Employee);
