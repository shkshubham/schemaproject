import React, { Component } from 'react';
import './App.css';
import { Container, Row, Col } from 'react-bootstrap';
import Employee from '../Employee';
class App extends Component {
  render() {
    return (
       <Container>
          <Employee />
        </Container>
    );
  }
}

export default App;
