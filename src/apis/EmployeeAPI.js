import {
    hostURL,
} from './api'
import request from 'superagent';

export const getEmployeeAPI = (employee_id) => {
    return request.get(`${hostURL}/Employee?userId=${employee_id}`);
} 
export const getEmployeeTokenAPI = (data) => {
    const {
        username,
        password
    } = data;

    return request.post(`http://challenge.schema.rocks/token`).type('form').send({
        grant_type: "password",
        username,
        password
    })
} 


export const editEmpolyeeAPI = (_data) => {
    const {
        token,
        employee_id,
        data
    } = _data;
    console.log("sadsad",data)
    return request.post(`${hostURL}/Employee?userId=${employee_id}`).set({
        "Authorization": `Bearer ${token}`
    }).set({
        "Content-Type": "application/json"
    }).send(data);
} 